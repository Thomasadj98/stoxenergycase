let mobileMenu = document.querySelector(".mobile-menu-container");

let hamburger = document.querySelector("#hamburger-icon");
let times = document.querySelector("#times-icon");

hamburger.addEventListener("click", () => {
  mobileMenu.style.display = "block";
  hamburger.style.display = "none";
  times.style.display = "block";
});

times.addEventListener("click", () => {
  mobileMenu.style.display = "none";
  hamburger.style.display = "block";
  times.style.display = "none";
});

/* Mobile menu */

let slider = document.querySelector(".swiper-container");
let innerSlider = document.querySelector(".swiper-container_inner");

let pressed = false;
let startx;
let x;

slider.addEventListener("mousedown", (e) => {
  pressed = true;
  startx = e.offsetX - innerSlider.offsetLeft;
  slider.style.cursor = "grabbing";
});

slider.addEventListener("mouseenter", () => {
  slider.style.cursor = "pointer";
});

slider.addEventListener("mouseup", () => {
  slider.style.cursor = "grab";
});

window.addEventListener("mouseup", () => {
  pressed = false;
});

slider.addEventListener("mousemove", (e) => {
  if (!pressed) return;
  e.preventDefault();

  x = e.offsetX;

  innerSlider.style.left = `${x - startx}px`;

  checkBoundary();
});

function checkBoundary() {
  let outer = slider.getBoundingClientRect();
  let inner = innerSlider.getBoundingClientRect();

  if (parseInt(innerSlider.style.left) > 0) {
    innerSlider.style.left = "0px";
  } else if (inner.right < outer.right) {
    innerSlider.style.left = `-${inner.width - outer.width}px`;
  }
}

// Touch screen events

slider.addEventListener("touchstart", (e) => {
  pressed = true;
  startx = e.touches[0].pageX - innerSlider.offsetLeft;
  console.log(innerSlider.offsetLeft);
});

window.addEventListener("touchend", () => {
  pressed = false;
  console.log("touch end");
});

slider.addEventListener("touchmove", (e) => {
  if (!pressed) return;
  e.preventDefault();

  x = e.touches[0].pageX;

  innerSlider.style.left = `${x - startx}px`;

  checkBoundary();
});

function checkBoundary() {
  let outer = slider.getBoundingClientRect();
  let inner = innerSlider.getBoundingClientRect();

  if (parseInt(innerSlider.style.left) > 0) {
    innerSlider.style.left = "0px";
  } else if (inner.right < outer.right) {
    innerSlider.style.left = `-${inner.width - outer.width}px`;
  }
}

//  ACCORDION MENU

// Daily category

let dailyAccordionItems = document.querySelector(".accordion-items_1");
let dailyArrowDown = document.querySelector(".arrow-down_1");
let dailyArrowUp = document.querySelector(".arrow-up_1");
let dailyHeaderAccordion = document.querySelector(".accordion-item__header_1");

dailyArrowDown.addEventListener("click", () => {
  dailyArrowDown.style.display = "none";
  dailyArrowUp.style.display = "block";
  dailyAccordionItems.style.display = "block";
  dailyHeaderAccordion.style.marginBottom = "20px";
});

dailyArrowUp.addEventListener("click", () => {
  dailyArrowDown.style.display = "block";
  dailyArrowUp.style.display = "none";
  dailyAccordionItems.style.display = "none";
  dailyHeaderAccordion.style.marginBottom = "0px";
});

// Sports category

let sportsAccordionItems = document.querySelector(".accordion-items_2");
let sportsArrowDown = document.querySelector(".arrow-down_2");
let sportsArrowUp = document.querySelector(".arrow-up_2");
let sportsHeaderAccordion = document.querySelector(".accordion-item__header_2");

sportsArrowDown.addEventListener("click", () => {
  sportsArrowDown.style.display = "none";
  sportsArrowUp.style.display = "block";
  sportsAccordionItems.style.display = "block";
  sportsHeaderAccordion.style.marginBottom = "20px";
});

sportsArrowUp.addEventListener("click", () => {
  sportsArrowDown.style.display = "block";
  sportsArrowUp.style.display = "none";
  sportsAccordionItems.style.display = "none";
  sportsHeaderAccordion.style.marginBottom = "0px";
});
